import java.net.Socket;

public class SocketPacketStruct {
    private final Socket socket;
    private final Packet packet;

    public SocketPacketStruct(Socket clientsocket, byte[] packet) {
        this.socket = clientsocket;
        this.packet = new Packet(packet);
    }


    public Socket getClientsocket()
    {
        return socket;
    }

    public Packet getPacket() {
        return packet;
    }
}
