import java.nio.ByteBuffer;

public class Packet {

    private final short type;
    private final short msgNo;
    private final short[] msglength;
    private final String[] msg;   //not binary
    private  short byteSumMsgs;

    public Packet(short type,short msgNo, String[] msg)
    {
        this.type=type;
        this.byteSumMsgs=0;
        this.msgNo=msgNo;
        this.msglength= new short[msgNo];
        this.msg = new String[msgNo];

        for(int i=0;i<msgNo;i++)
        {
            this.msg[i]=msg[i];
            this.msglength[i]=(short) Chat_Utilities.stringToBytesUTFCustom(msg[i]).length;
            byteSumMsgs+=msglength[i];
        }
    }


    public Packet(short type)
    {
        this.type=type;
        this.byteSumMsgs=0;
        this.msgNo=0;
        this.msglength= null;
        this.msg = null;
    }

    public Packet(byte[] fullPacket)
    {
        this.type=ByteBuffer.wrap(Chat_Utilities.cutArray(fullPacket,0,2)).getShort();
        this.msgNo=ByteBuffer.wrap(Chat_Utilities.cutArray(fullPacket,2,2)).getShort();

        if(this.msgNo!=0) {
                            this.msglength = new short[msgNo];
                            this.msg = new String[msgNo];
                            this.byteSumMsgs = 0;

                            for (int i = 0; i < msgNo; i++) {
                                this.msglength[i] = ByteBuffer.wrap(Chat_Utilities.cutArray(fullPacket, 4 + 2 * i + byteSumMsgs, 2)).getShort();

                                this.msg[i] = Chat_Utilities.bytesToStringUTFCustom(Chat_Utilities.cutArray(fullPacket, 4 + 2 * (i + 1) + byteSumMsgs, msglength[i]));

                                byteSumMsgs += this.msglength[i];

                            }
        }
        else
        {
            this.byteSumMsgs=0;
            this.msglength= null;
            this.msg = null;
        }
    }

    public byte[] encodeBinary() {
        byte[] propertiesArray = new byte[4+msgNo*2+this.byteSumMsgs];
        int sum=0;

        ByteBuffer buffer = ByteBuffer.allocate(2);

        buffer.putShort(this.type);
        propertiesArray= Chat_Utilities.copyByteArray(buffer.array(),propertiesArray,0,2,0);

        if(getMsgNo()!=0) {
                                buffer = ByteBuffer.allocate(2);
                                buffer.putShort(getMsgNo());
                                propertiesArray = Chat_Utilities.copyByteArray(buffer.array(), propertiesArray, 0, 2, 2);

                                for (int i = 0; i < getMsgNo(); i++) {
                                    buffer = ByteBuffer.allocate(2);
                                    buffer.putShort(getMsglength()[i]);
                                    propertiesArray = Chat_Utilities.copyByteArray(buffer.array(), propertiesArray, 0, 2, 4 + 2 * i + sum);

                                    propertiesArray = Chat_Utilities.copyByteArray(Chat_Utilities.stringToBytesUTFCustom(getMsg()[i]), propertiesArray, 0, Chat_Utilities.stringToBytesUTFCustom(getMsg()[i]).length, 4 + 2 * (i + 1) + sum);

                                    sum += Chat_Utilities.stringToBytesUTFCustom(getMsg()[i]).length;
                                }

        }
        return propertiesArray;
    }

    public short getMsgNo() {
        return msgNo;
    }

    public short[] getMsglength() {
        return msglength;
    }

    public String[] getMsg() {
        return msg;
    }

    public short getType() {
        return type;
    }

    public short getByteSumMsgs() {
        return byteSumMsgs;
    }
}
