import javafx.scene.input.KeyCode;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;

public class MsgWindow {
    private JPanel panel1;
    private JTextPane textPane1;
    private JButton button1;
    private JTextField textField1;
    private JButton refreshButton;
    private JComboBox comboBox1;
    private JTextField textField2;
    private JList list1;
    private String token;
    private int friendsCount;
    private HashMap<String,String> conversations;


    public MsgWindow(String token, Socket socket) {

        conversations=new HashMap<>();



        Thread t = new Thread(new Runnable() { // THREAD THAT UPDATES CONTINUOUSLY FRIEND LIST AND CHAT PANEL
            @Override
            public void run() {
                while(true)
                {
                    if(comboBox1.getSelectedItem()!=null)
                    {
                        if(ChatWindow.getChatText(comboBox1.getSelectedItem().toString())!=null)
                        if(textPane1.getText()!=ChatWindow.getChatText(comboBox1.getSelectedItem().toString()))
                        textPane1.setText(ChatWindow.getChatText(comboBox1.getSelectedItem().toString()));

                    }


                    if(friendsCount!=ChatWindow.getFriendCount())
                    {comboBox1.removeAllItems();

                    for(int i=0;i<ChatWindow.getFriendCount();i++)
                    {
                        comboBox1.addItem(ChatWindow.getFriends()[i]);

                    }
                    friendsCount=ChatWindow.getFriendCount();
                    }

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }



                }
            }
        });

        t.start();

        this.token=token;




        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {



                Packet p = new Packet(Packet_Defines_Client.SEND_MSG,(short)3,new String[]{comboBox1.getSelectedItem().toString(),token,textField1.getText()});

                try {
                    Chat_Utilities.sendBytes(p.encodeBinary(),0,p.encodeBinary().length,socket);

                } catch (IOException e) {
                    e.printStackTrace();
                }

                textField1.setText("");
            }
        });
        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                Packet p = new Packet(Packet_Defines_Client.REQUEST_FRIEND_ADD,(short)2,new String[]{token,textField2.getText()});

                try {
                    Chat_Utilities.sendBytes(p.encodeBinary(),0,p.encodeBinary().length,socket);

                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        });
        textField1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent keyEvent) {
                if(keyEvent.getKeyCode() == KeyEvent.VK_ENTER)
                {

                    Packet p = new Packet(Packet_Defines_Client.SEND_MSG,(short)3,new String[]{comboBox1.getSelectedItem().toString(),token,textField1.getText()});

                    try {
                        Chat_Utilities.sendBytes(p.encodeBinary(),0,p.encodeBinary().length,socket);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    textField1.setText("");
                }
            }
        });


        comboBox1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                textPane1.setText("");
            }
        });
    }

    public JPanel getPanel1() {
        return panel1;
    }
}
