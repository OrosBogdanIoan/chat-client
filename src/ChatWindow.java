import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ChatWindow {
    private JPanel panel1;
    private  JTextField textField1;
    private JTextField textField2;
    private JButton button1;
    private JButton registerButton;
    private JTextField ipTextField;

    private static JFrame window;
    private  static Socket serverSocket;
    private static boolean serverSet;
    private static int friendCount;
    private static String[] friends;
    private static String  name;
    private static HashMap<String,String> convo;

    private static String getConversation(String user)
    {
        return convo.get(user);
    }

    public static String getChatText(String user){   return convo.get(user);  }

    public static String[] getFriends(){
        return friends;
    }

    public static int getFriendCount()
    {
        return friendCount;
    }

    public ChatWindow() {

        convo= new HashMap<>();
        try {
            Scanner input = new Scanner(new File("default.config"));

            serverSocket = new Socket(input.nextLine(), 4449);
        } catch (IOException e) {

            JOptionPane.showMessageDialog(null,"Initial connection failed. Server might be under maintenance");
            System.exit(0);
        }



        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {



                try {
                    Packet p = new Packet(Packet_Defines_Client.REQUEST_LOGIN,(short)2,new String[]{textField1.getText(),textField2.getText()});

                    Chat_Utilities.sendBytes(p.encodeBinary(),0,p.encodeBinary().length,serverSocket);

                    name=textField1.getText();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                if(textField1.getText().length()>0 && textField2.getText().length()>0) {
                    try {
                        Packet p = new Packet(Packet_Defines_Client.REQUEST_REGISTER, (short) 2, new String[]{textField1.getText(), textField2.getText()});

                        Chat_Utilities.sendBytes(p.encodeBinary(), 0, p.encodeBinary().length, serverSocket);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "Credential fields cannot be empty");
                }
            }
        });

    }


    public static  void disconnectNicely()
    {
        try {
            Packet p = new Packet(Packet_Defines_Client.REQUEST_DISCONNECT);

            Chat_Utilities.sendBytes(p.encodeBinary(),0,p.encodeBinary().length,serverSocket);


        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public static void main(String[] args)
    {

        window = new JFrame("ChatWindow");
        //Hide icon
        Image icon = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB_PRE);
        window.setIconImage(icon);


        //Make it appear in the middle of the screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        window.setLocation(dim.width/2-window.getSize().width/2, dim.height/2-window.getSize().height/2);


        window.setContentPane(new ChatWindow().panel1);
        window.setDefaultCloseOperation(window.DO_NOTHING_ON_CLOSE);

        window.pack();
        window.setVisible(true);

while(true)
{
        Packet p = null;
        try {
            p = new Packet(Chat_Utilities.readBytes(serverSocket));
        } catch (IOException e) {

            JOptionPane.showMessageDialog(null, "You have been  disconnected");
            System.exit(0);
        }

        switch (p.getType()) {
            case Packet_Defines_Server.LOGIN_FAIL_CREDENTIALS: {
                JOptionPane.showMessageDialog(null, "Wrong credentials");
                break;
            }
            case Packet_Defines_Server.GET_FRIEND_LIST_FAILED_INVALID_TOKEN: {
                JOptionPane.showMessageDialog(null, "Could not receive friendlist, invalid authentication token used");
                break;
            }
            case Packet_Defines_Server.GET_FRIEND_LIST_FAILED_NO_FRIENDS: {
                JOptionPane.showMessageDialog(null, "You don't have any friends yet, make sure to connect with your buddies!");
                break;
            }
            case Packet_Defines_Server.GET_FRIEND_LIST_FAILED_SERVICE: {
                JOptionPane.showMessageDialog(null, "There is a serversided issue, could not get friendlist");
                break;
            }

            case Packet_Defines_Server.REGISTER_SUCCESS: {
                JOptionPane.showMessageDialog(null, "You have been successfully registered.");
                break;
            }
            case Packet_Defines_Server.LOGIN_FAILED_USER_ALREADY_LOGGED: {
                JOptionPane.showMessageDialog(null, "User is already logged in");
                break;
            }
            case Packet_Defines_Server.LOGIN_FAIL_SERVICE: {
                JOptionPane.showMessageDialog(null, "Service temporarily not available");
                break;
            }
            case Packet_Defines_Server.REGISTER_FAIL_SERVICE: {
                JOptionPane.showMessageDialog(null, "Register failed due to serversided issue");
                break;
            }
            case Packet_Defines_Server.REGISTER_FAIL_USER_DUPLICATE: {
                JOptionPane.showMessageDialog(null, "Username already exists in.");
                break;
            }

            case Packet_Defines_Server.SEND_MSG_FAIL_INVALID_TOKEN: {
                JOptionPane.showMessageDialog(null, "Could not send message, invalid authentication token.");
                break;
            }
            case Packet_Defines_Server.SEND_MSG_FAIL_SERVICE: {
                JOptionPane.showMessageDialog(null, "Could not send message, a server-sided issue has arisen");
                break;
            }

            case Packet_Defines_Server.INVALID_CLIENT_PACKET: {
                JOptionPane.showMessageDialog(null, "The server doesn't recognize the packet you've sent.");
                break;
            }
            case Packet_Defines_Server.LOGIN_SUCCESS: {
                String token = null;
                try {
                    token = new Packet(Chat_Utilities.readBytes(serverSocket)).getMsg()[0];
                    Packet r = new Packet(Packet_Defines_Client.REQUEST_FRIEND_LIST, (short) 1, new String[]{token});


                    Chat_Utilities.sendBytes(r.encodeBinary(), 0, r.encodeBinary().length, serverSocket);


                    window.setContentPane(new MsgWindow(token, serverSocket).getPanel1());
                    window.setSize(500, 400);
                    window.setTitle(window.getTitle() + " - " + name);

                    window.addWindowListener(new WindowAdapter() {
                        @Override
                        public void windowClosing(WindowEvent event) {
                            disconnectNicely();
                            System.exit(0);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    break;
                }

            }
            case Packet_Defines_Server.RECEIVED_MSG_SUCCESS: {
                if (convo.get(p.getMsg()[0]) == null) {
                    convo.put(p.getMsg()[0], p.getMsg()[0] + ":" + p.getMsg()[1] + "\n");
                } else {
                    convo.put(p.getMsg()[0], convo.get(p.getMsg()[0]) + p.getMsg()[0] + ":" + p.getMsg()[1] + "\n");
                }
                break;
            }
            case Packet_Defines_Server.GET_FRIEND_LIST_SUCCESS: {
                friends = p.getMsg();
                friendCount = p.getMsgNo();

                break;
            }

            case Packet_Defines_Server.SELF_MESSAGE: {
                if (convo.get(p.getMsg()[0]) == null) {
                    convo.put(p.getMsg()[0], name + ":" + p.getMsg()[1] + "\n");
                } else {
                    convo.put(p.getMsg()[0], convo.get(p.getMsg()[0]) + name + ":" + p.getMsg()[1] + "\n");
                }
                break;
            }
            case Packet_Defines_Server.CONFIRM_DISCONNECT: {
                System.out.println("DISCONNECT FROM SERVER");
                System.exit(0);
                break;
            }

            case Packet_Defines_Server.SEND_MSG_FAIL_USER_NOT_IN_FRIENDLIST: {
                JOptionPane.showMessageDialog(null, "User not in friendship list");
                break;
            }

            case Packet_Defines_Server.SEND_MSG_FAIL_USER_OFFLINE: {
                JOptionPane.showMessageDialog(null, "User is offline");
                break;
            }

            case Packet_Defines_Server.ADD_FRIEND_USER_ALREADY_FRIEND: {
                JOptionPane.showMessageDialog(null, "Already friends with that user");
                break;
            }
            case Packet_Defines_Server.ADD_FRIEND_USER_NOT_ONLINE: {
                JOptionPane.showMessageDialog(null, "User is not online");
                break;
            }
            case Packet_Defines_Server.ADD_FRIEND_USER_FAIL_SERVICE: {
                JOptionPane.showMessageDialog(null, "Friendship request failed due to serversided issue");
                break;
            }
            default:
                break;
        }



    } // while true





    }


    }
